# embeddingsims

edit: a simpler solution for this tutorial can be found at huggingface.co/sentence-transformers/multi-qa-MiniLM-L6-cos-v1    it uses python sentence-transformers module and is a similar example, but even simpler (check out file simpler.py)

From a list of 15 sentences (15 most important improvements of C++14), let's use openai to tell us which ones are the closest in "meaning" to the numbered 7 (talking about constexpr)
This list was brought by openai itself, during a chatgpt session, and I noticed statements 6, 7 and 9 were all about constexpr. With 7 and 9 being particularly close in meaningness (I suspect openai to be guilty of a redundancy in that particular case, so let's check if I am true about it, by using openai itself to help us verify its guilt).
sentences are put in a string list. Our "pivot" string will be the 7th point, which is indexed as element number 6 in our list.
In the end, and if my suspicions are correct, we should end up with closest elements of element 6 should be elements 6, 8 and 5 (== 7, 9 and 6 minus one for indexation)

bonus: visualization according to https://github.com/openai/openai-cookbook/blob/main/examples/Visualizing_embeddings_in_2D.ipynb 
or/and https://github.com/openai/openai-cookbook/blob/main/examples/Clustering.ipynb
or simply make use of provided methods at end of embeddings_utils.py

some points to note about openai embeddings:
	embeddings measure topical similarity more than logical similarity (e.g., The sky is blue is very close to The sky is not blue)
    punctuation affects embeddings (e.g., THE. SKY. IS. BLUE! is only third closest to The sky is blue)
    within-language pairs are stronger than across-language pairs (e.g., El cielo as azul is closer to El cielo es rojo than to The sky is blue)
    distance metric (we recommend cosine)
    embedding model (we recommend text-embedding-ada-002 for most use cases, as of May 2023)

openai module refs can be found here:
~/.local/lib/python3.7/site-packages/openai/

and here is how to retrieve embeddings for a sentence on cmdline using curl:
curl https://api.openai.com/v1/embeddings  -H "Content-Type: application/json" -H "Authorization: Bearer $OPENAI_API_KEY" -d '{"input": "what goes around", "model": "text-embedding-ada-002"}' 

for iterative development, it was better to use python interactively, and save the session (using dill module), in order not to use too much my openai api key for my subscription is of limited use...

requirements:
- basically all modules as iterated at beginning of python source file
- python3 idk which version...
- openai api key
	+ subscribe to platform.openai.com
	+ put it into OPENAI_API_KEY of your local environment variable
	+ the usage that is going to be made of it is very limited, though I wouldn't give a precise approximation: here we are working with 15 sentences, and retrieve their embeddings, once

usage:
$ python3 oaapi.py

results:
only chebyshev distance measure metric gave a result not expected, -> ?
but the rest is unanimously confirming that sentences 7 and 9 are closest in meaning
Nevertheless, the distance between those two sentences are high (should not be in my opinion) and this probably explains why openai considered them as different statements worth mentionning separately. I find this little experiment interesting for learning some stuffs, and undestanding how openai embeddings system works, and how it is limited (though it is probably limited by many other things...)




gitlab README generated content

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/millesabords/embeddingsims.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/millesabords/embeddingsims/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
