from os import environ
from typing import List
import numpy as np
import openai
from openai.embeddings_utils import get_embedding

openai_api_key=environ.get('OPENAI_API_KEY')

""" #simple chatgpt usage example: """
#chat_completion = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=[{"role": "user", "content": "what are the 5 best NBA teams at the moment?"}])
#print(chat_completion.choices[0]["message"]["content"])
#print("embeddings: ",chat_completion.choices[0]["embedding"])

""" #retrieving existing models: """
#models = openai.Model.list()
#print(models.data[0].id)

""" old way of using api: """
#llm = OpenAI(openai_api_key = openai_api_key, model_name = 'text-davinci-003', temperature=1)
#gpt-3.5-turbo',
#        temperature=1)
#print(llm("(only jokes alowed) what's in the chimney?"))
#print(llm("translate me the following sentence from english to traditionnal chinese (using both hanzi and pinyin): I love earl grey tea, it is my favorite drink right now."))

""" retrieving embeddings for a list of sentences """
strList:List[str]=[
        'Binary literals: C++14 introduced the ability to represent binary numbers using the prefix `0b` followed by a sequence of 0s and 1s. For example, `0b101010` represents the decimal number 42.',
        'Digit separators: C++14 allows the use of single quotes as digit separators in numeric literals to improve readability. For example, `1\'000\'000` is equivalent to `1000000`.',
        'Return type deduction for functions: C++14 allows the return type of a function to be deduced automatically based on the type of the expression returned. This eliminates the need to explicitly specify the return type in many cases.',
        'Variable templates: C++14 introduced the concept of variable templates, which are similar to function templates but for variables. This allows you to define generic variables that can be instantiated with different types.',
        'Generic lambdas: C++14 allows lambda functions to be defined with auto parameters, enabling the creation of generic lambdas that can accept arguments of any type.',
        'constexpr member functions: C++14 allows member functions to be declared as `constexpr`, indicating that they can be evaluated at compile-time if called with constant expressions.',
        'Relaxing restrictions on constexpr functions: C++14 relaxed some of the restrictions on `constexpr` functions, allowing them to contain a wider range of statements, including loops and conditional statements.',
        'Variable-sized arrays with automatic storage duration: C++14 introduced the ability to declare arrays with a size determined at runtime using the `std::dynarray` class template.',
        'Extended `constexpr` support: C++14 extended the range of expressions that can be evaluated at compile-time, including more standard library functions and operations.',
        'Improved `std::make_unique`: C++14 added `std::make_unique` as a counterpart to `std::make_shared`, providing a convenient way to create `std::unique_ptr` objects with dynamic memory allocation.',
        '`std::quoted` for I/O manipulators: C++14 introduced the `std::quoted` function, which allows you to apply quoting and escaping to strings when using I/O manipulators like `std::getline` and `std::setw`.',
        '`std::exchange` for atomic operations: C++14 added the `std::exchange` function, which provides a safe and efficient way to perform atomic read-modify-write operations on variables of type `std::atomic`.',
        '`std::experimental::optional`: C++14 introduced the `std::experimental::optional` class template, which provides a way to represent optional values without using pointers or other indirection mechanisms.',
        '`std::experimental::string_view`: C++14 introduced the `std::experimental::string_view` class template, which provides a lightweight, non-owning view of a string.',
        '`std::experimental::filesystem`: C++14 introduced the `std::experimental::filesystem` library, which provides a set of classes and functions for working with files and directories in a platform-independent way.'
        ]
#print(openai.embeddings_utils.get_embeddings(strList))
#lengths = [len(sublist) for sublist in a_list_of_lists]

myEmbeddings=openai.embeddings_utils.get_embeddings(strList)
# "cosine": spatial.distance.cosine,
# "L1": spatial.distance.cityblock, cityblock = manhattan or taxicab
# "L2": spatial.distance.euclidean,
# "Linf": spatial.distance.chebyshev,
# or make use of scipy lib:     1. ``Y = cdist(XA, XB, 'euclidean')``
# check out at /home/ouam/.local/lib/python3.7/site-packages/scipy/spatial/distance.py
dists_cosine=openai.embeddings_utils.distances_from_embeddings(myEmbeddings[6], myEmbeddings, distance_metric='cosine')
dists_L1=openai.embeddings_utils.distances_from_embeddings(myEmbeddings[6], myEmbeddings, distance_metric='L1')
dists_L2=openai.embeddings_utils.distances_from_embeddings(myEmbeddings[6], myEmbeddings, distance_metric='L2')
dists_Linf=openai.embeddings_utils.distances_from_embeddings(myEmbeddings[6], myEmbeddings, distance_metric='Linf')
# embeddings_utils.indices_of_nearest_neighbors_from_distances is just a forward method to numpy.argsort
# (tries to sort by values, but instead of returning sorted values, returns the respective indexes of the sorted values), so let's use argsort instead
res_cosine=np.argsort(dists_cosine)
res_manhattan=np.argsort(dists_L1)
res_euclidean=np.argsort(dists_L2)
res_chebyshev=np.argsort(dists_Linf)

print("indexes of closest to number 6, according to cosine distance measures: ", res_cosine)
print("indexes of closest to number 6, according to manhattan distance measures: ", res_manhattan)
print("indexes of closest to number 6, according to euclidean distance measures: ", res_euclidean)
print("indexes of closest to number 6, according to chebyshev distance measures: ", res_chebyshev)


print("dists_cosine=", dists_cosine)
print("dists_L1=", dists_L1)
print("dists_L2=", dists_L2)
print("dists_=Linf", dists_Linf)
