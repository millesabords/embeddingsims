import openai
from langchain.llms import OpenAI
from langchain import PromptTemplate
from os import environ

openai_api_key=environ.get('OPENAI_API_KEY')

llm = OpenAI(openai_api_key = openai_api_key, model_name = 'text-davinci-003', temperature=1)

#gpt-3.5-turbo',
#        temperature=1)

#print(llm("(only jokes alowed) what's in the chimney?"))
print(llm("(only jokes alowed) what's in the chimney?"))
#print(llm("translate me the following sentence from english to traditionnal chinese (using both hanzi and pinyin): I love earl grey tea, it is my favorite drink right now."))

#### using prompt template submodule
#template_mult_var = "you are a helpful assistant to translate the following sentence from {input_lang} to {output_lang}. here's sentence: {sentence}"
#prompt_translator = PromptTemplate(input_variables = ["input_lang", "output_lang", "sentence"], template = template_mult_var)
#prompt_translator.format(input_lang = 'English', output_lang='Traditionnal Mandarin', sentence='i love earl grey tea, it is my favorite drink right now.')

#print("prompttemplated request = ", prompt_translator.template, "\n")
#print("prompttemplated answer = ", llm(prompt_translator.serialize()), "\n")


